import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProjectOwnerRoutingModule} from './project-owner-routing.module';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {SharedModule} from '../shared/shared.module';
import {CreateProjectDialogComponent} from './dialogs/create-project-dialog/create-project-dialog.component';


@NgModule({
  declarations: [DashboardComponent, CreateProjectDialogComponent],
  imports: [
    CommonModule,
    ProjectOwnerRoutingModule,
    SharedModule,
  ]
})
export class ProjectOwnerModule {
}
