import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {FormValidation} from '../../../shared/classes/form-validation';
import {HttpService} from '../../../shared/services/http.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AlertDialogComponent} from '../../../shared/dialogs/alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-create-project-dialog',
  templateUrl: './create-project-dialog.component.html',
  styleUrls: ['./create-project-dialog.component.scss']
})
export class CreateProjectDialogComponent extends FormValidation implements OnInit {
  @ViewChild('filePicker', {static: false}) filePicker: ElementRef;
  formData: FormData = new FormData();
  projectTitle = new FormControl('', [
    Validators.required,
    Validators.maxLength(50),
  ]);
  projectDescription = new FormControl('', [
    Validators.required,
    Validators.maxLength(100),
  ]);
  projectCategory = new FormControl('', [
    Validators.required,
  ]);
  projectDetail = new FormControl('', [
    Validators.required,
  ]);

  categories: any = [];
  project: any;
  isLoading: boolean;

  constructor(private httpService: HttpService,
              private dialog: MatDialog,
              private dialogRef: MatDialogRef<any>,
              @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super();
  }

  ngOnInit(): void {
    this.categories = this.data.categories;
    this.project = this.data.project || null;
    if (this.project) {
      this.projectDescription.setValue(this.project.description);
      this.projectTitle.setValue(this.project.title);
      this.projectCategory.setValue(this.project.category._id);
      this.projectDetail.setValue(this.project?.info);
    }

  }

  handleClickButton() {
    this.formData.append('title', this.projectTitle.value);
    this.formData.append('description', this.projectDescription.value);
    this.formData.append('category', this.projectCategory.value);
    this.formData.append('detail', this.projectDetail.value);
    const body = {
      title: this.formData.get('title'),
      description: this.formData.get('description'),
      category: this.formData.get('category'),
      attachment: this.formData.get('attachment'),
      info: this.formData.get('detail'),
    };

    // Update
    if (this.project) {
      return this.update(body);
    }
    // Create
    this.create(body);
  }

  openFilePickerPopUp() {
    this.filePicker.nativeElement.click();
  }

  fileInputChange(e) {
    this.formData.append('attachment', e.target.files);
  }

  private create(body: { attachment: File | string; description: File | string; title: File | string; category: File | string }) {
    this.isLoading = true;
    const sub = this.httpService
      .post(`/project-owner/project`, body)
      .subscribe(() => {
        this.isLoading = false;
        this.dialogRef.close({
          success: true,
        });
      }, ({error: {message}}) => {
        this.dialog.open(AlertDialogComponent, {
          data: {
            message,
          }
        });
        this.isLoading = false;
      })
      .add(function() {
        sub.unsubscribe();
      });
  }

  private update(body: { attachment: File | string; description: File | string; title: File | string; category: File | string }) {
    this.isLoading = true;
    const sub = this.httpService
      .put(`/project-owner/project/${this.project._id}`, body)
      .subscribe(() => {
        this.isLoading = false;
        this.dialogRef.close({
          success: true,
        });
      }, ({error: {message}}) => {
        this.dialog.open(AlertDialogComponent, {
          data: {
            message,
          }
        });
      })
      .add(function() {
        sub.unsubscribe();
      });
  }
}
