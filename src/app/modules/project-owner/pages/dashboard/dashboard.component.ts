import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpService } from '../../../shared/services/http.service';
import { CreateProjectDialogComponent } from '../../dialogs/create-project-dialog/create-project-dialog.component';
import { AlertDialogComponent } from '../../../shared/dialogs/alert-dialog/alert-dialog.component';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from '../../../shared/dialogs/confirm-dialog/confirm-dialog.component';
import { ProjectService } from '../../../shared/services/project.service';
import { UserService } from '../../../shared/services/user.service';
import { Socket } from 'ngx-socket-io';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  projects = this.socket.fromEvent<any[]>('projects');
  categories: any = [];
  isLoading = false;
  user;
  subs: Subscription[] = [];

  constructor(
    private httpService: HttpService,
    private dialog: MatDialog,
    private router: Router,
    private projectService: ProjectService,
    private userService: UserService,
    private socket: Socket,
    private authService: AuthService
  ) {}

  ngOnDestroy(): void {
    this.subs.map((sub) => sub.unsubscribe());
  }

  createProject(): void {
    const sub = this.dialog
      .open(CreateProjectDialogComponent, {
        data: {
          categories: this.categories,
        },
      })
      .afterClosed()
      .subscribe((res) => {
        if (res.success) {
          this.router
            .navigateByUrl('', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/project_owner']);
            });
        }
      })
      // tslint:disable-next-line:only-arrow-functions
      .add(function (): void {
        sub.unsubscribe();
      });
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.socket.emit('project-owner', this.authService.parseToken().sub.uuid);
    this.subs.push(
      this.projects.subscribe((_) => {
        this.isLoading = false;
      })
    );
    // this.loadOwnProject();
    this.loadUser();
    this.loadCategory();
  }

  importProject(): void {
    console.log('import me');
  }

  deleteProject(project: any): void {
    const sub1 = this.dialog
      .open(ConfirmDialogComponent, {
        data: {
          message: 'Are you sure to do this ?',
        },
      })
      .afterClosed()
      .subscribe((res) => {
        if (res.deleteMe) {
          const sub = this.httpService
            .delete(`/project-owner/project/${project._id}`)
            .subscribe(
              ({ message }) => {
                const sub2 = this.dialog
                  .open(AlertDialogComponent, {
                    data: {
                      message,
                      title: 'Success',
                    },
                  })
                  .afterClosed()
                  .subscribe(() => {
                    this.router
                      .navigateByUrl('', { skipLocationChange: true })
                      .then(() => {
                        this.router.navigate(['/project_owner']);
                      });
                  })
                  // tslint:disable-next-line:only-arrow-functions
                  .add(function (): void {
                    sub2.unsubscribe();
                  });
              },
              ({ error: { message } }) => {
                this.dialog.open(AlertDialogComponent, {
                  data: {
                    message,
                  },
                });
              }
            )
            // tslint:disable-next-line:only-arrow-functions
            .add(function (): void {
              sub.unsubscribe();
            });
        }
      })
      // tslint:disable-next-line:only-arrow-functions
      .add(function (): void {
        sub1.unsubscribe();
      });
  }

  editProject(project: any): void {
    const sub = this.dialog.open(CreateProjectDialogComponent, {
      data: {
        categories: this.categories,
        project
      }
    }).afterClosed()
      .subscribe((res) => {
        if (!res?.success) {
          return;
        }
        this.router.navigateByUrl('', {skipLocationChange: true}).then(() => {
          this.router.navigate(['/project_owner']);
        });
      })
      // tslint:disable-next-line:only-arrow-functions
      .add(function(): void {
        sub.unsubscribe();
      });
  }

  private loadUser(): void {
    this.isLoading = true;
    const sub2 = this.userService
      .getUser()
      .subscribe(
        (res) => {
          this.user = res.data;
          this.isLoading = false;
        },
        ({ error: { message } }) => {
          this.isLoading = false;
        }
      )
      // tslint:disable-next-line:only-arrow-functions
      .add(function (): void {
        sub2.unsubscribe();
      });
  }

  private loadOwnProject(): void {
    this.isLoading = true;
    const sub = this.projectService
      .getOwn()
      .subscribe(
        (res) => {
          this.projects = res.data;
          console.log(this.projects);
          this.isLoading = false;
        },
        ({ error: { message } }) => {
          console.log(message);
          this.isLoading = false;
        }
      )
      // tslint:disable-next-line:only-arrow-functions
      .add(function (): void {
        sub.unsubscribe();
      });
  }

  private loadCategory(): void {
    const sub1 = this.httpService
      .get('/category')
      .subscribe((res) => {
        this.categories = res.data;
      })
      // tslint:disable-next-line:only-arrow-functions
      .add(function (): void {
        sub1.unsubscribe();
      });
  }
}
