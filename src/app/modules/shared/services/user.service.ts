import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private httpService: HttpService) {
  }

  getUser(): Observable<any> {
    return this.httpService
      .get('/get-user');
  }
}
