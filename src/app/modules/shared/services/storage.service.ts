import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor(private cookie: CookieService, private jwtService: JwtHelperService) {
  }

  get(key: string): string {
    return this.cookie.get(key);
  }

  set(key: string, value: string): void {
    this.cookie.set(key, value);
  }

  getObject(key: string): object {
    try {
      return JSON.parse(this.cookie.get(key));
    } catch (e) {
      return null;
    }
  }

  setObject(key: string, value: any): void {
    let exp;
    if (value?.hasOwnProperty('token')) {
      exp = this.jwtService.decodeToken(value.token)?.exp;
    }
    this.cookie.set(key, JSON.stringify(value), {
      path: '/',
      expires: exp,
      sameSite: 'None',
      secure: true
    });
  }
}
