import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(
    private httpService: HttpService
  ) {
  }

  getAll(): Observable<any> {
    return this.httpService.get(`/sponsor/projects`);
  }

  vote(projectId: string, role: string): Observable<any> {
    return this.httpService.patch(`/${role}/project/vote/${projectId}`, {});
  }

  invest(projectId: string): Observable<any> {
    return this.httpService.patch(`/sponsor/project/invest/${projectId}`, {});
  }

  getOwn(): Observable<any> {
    return this.httpService.get(`/project-owner/projects/own`);
  }

  getWithoutDetail(): Observable<any> {
    return this.httpService.get(`/public/projects`);
  }
}
