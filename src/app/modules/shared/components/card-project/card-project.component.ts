import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-card-project',
  templateUrl: './card-project.component.html',
  styleUrls: ['./card-project.component.scss']
})
export class CardProjectComponent implements OnInit {
  @Input() public project;
  @Input() public user;
  @Output() public support = new EventEmitter();
  @Output() public comment = new EventEmitter();
  @Output() public vote = new EventEmitter();
  // tslint:disable-next-line:ban-types
  alreadyLiked: boolean;
  alreadyInvest: boolean;

  constructor() {
  }

  ngOnInit(): void {
    // This means that the user is not PUBLIC
    if (this.user) {
      this.alreadyLiked = this.project?.votes.sponsor.find(s => s.user === this.user?._id) || false;
      this.alreadyInvest = this.project?.investor.find(investor => investor._id === this.user?._id);
    }
  }

  supportProject(project): void {
    this.support.emit(project);
  }

  commentProject(project): void {
    this.comment.emit(project);
  }

  voteProject(project): void {
    this.vote.emit({project, user: !!this.user});
  }
}
