import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../auth/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-logout',
  template: `
    <app-loader></app-loader>
  `,
  styles: []
})
export class LogoutComponent implements OnInit {

  constructor(private authService: AuthService, private  router: Router) {
  }

  ngOnInit(): void {
    this.authService.logout();
    this.router.navigateByUrl('/auth/login');
  }

}
