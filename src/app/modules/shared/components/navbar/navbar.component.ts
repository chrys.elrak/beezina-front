import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../auth/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
  }

  logout(): void {
    this.authService.logout();
    this.router.navigateByUrl('/auth/login');
  }

  navigate(link: string): void {
    switch (link) {
      case 'projects':
        this.router.navigateByUrl('/projects');
        break;
      case 'dashboard':
        const url: string = '/' + this.role;
        this.router.navigateByUrl(url);
        break;
      case 'login':
        this.router.navigateByUrl('/auth/login');
        break;
      case 'register':
        this.router.navigateByUrl('/auth/register');
        break;
      default:
        this.router.navigateByUrl('/');
    }
  }

  get role(): string {
    return this.authService.getRole();
  }

  get hasUserLogged(): boolean {
    return this.authService.isAuthenticated();
  }
}
