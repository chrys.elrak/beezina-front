import {Component, Input, OnInit} from '@angular/core';
import moment from 'moment';

@Component({
  selector: 'app-card-comment',
  templateUrl: './card-comment.component.html',
  styleUrls: ['./card-comment.component.scss']
})
export class CardCommentComponent implements OnInit {
  @Input() public comments: any;
  @Input() public user;
  moment = moment;

  constructor() {
  }

  ngOnInit(): void {
  }

}
