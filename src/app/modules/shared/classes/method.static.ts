import TokenPayload from '../interfaces/ItokenPayload';
import decode from 'jwt-decode';

export default class MethodStatic {

  public static get role(): string {
    const tokenPayload: TokenPayload = decode(localStorage.getItem('token'));
    return tokenPayload.sub.role;
  }
}
