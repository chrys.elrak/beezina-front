import {AbstractControl, FormControl} from '@angular/forms';

export class FormValidation {
  getErrorMessage(field: AbstractControl | FormControl, label: string, match = false) {
    if (field.hasError('required')) {
      return 'You must enter a value';
    }
    return field.invalid && !match ? 'Not a valid ' + label : match ? 'Password not match' : '';
  }

}
