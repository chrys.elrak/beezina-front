import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpService} from './services/http.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
import {AlertDialogComponent} from './dialogs/alert-dialog/alert-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {JWT_OPTIONS, JwtHelperService} from '@auth0/angular-jwt';
import {Error404Component} from './pages/error404/error404.component';
import {LogoutComponent} from './components/logout/logout.component';
import {HttpConfigInterceptor} from './interceptor/http-config-interceptor.service';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatMenuModule} from '@angular/material/menu';
import {NavbarComponent} from './components/navbar/navbar.component';
import {LayoutModule} from '@angular/cdk/layout';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTabsModule} from '@angular/material/tabs';
import {ConfirmDialogComponent} from './dialogs/confirm-dialog/confirm-dialog.component';
import {QuillModule} from 'ngx-quill';
import {CookieService} from 'ngx-cookie-service';
import {RouterModule} from '@angular/router';
import {ProfileComponent} from './pages/profile/profile.component';
import {LoaderComponent} from './components/loader/loader.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {CardProjectComponent} from './components/card-project/card-project.component';
import {UserService} from './services/user.service';
import {CardCommentComponent} from './components/card-comment/card-comment.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AlertDialogComponent,
    Error404Component,
    LogoutComponent,
    NavbarComponent,
    ConfirmDialogComponent,
    ProfileComponent,
    LoaderComponent,
    CardProjectComponent,
    CardCommentComponent,
  ],
  imports: [
    CommonModule, FormsModule, HttpClientModule,
    ReactiveFormsModule, FormsModule, MatToolbarModule, MatIconModule,
    MatButtonModule, MatInputModule, MatFormFieldModule, MatCardModule,
    MatCheckboxModule, MatSelectModule, MatDialogModule, MatDividerModule,
    MatGridListModule, MatMenuModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule, MatExpansionModule, MatTabsModule,
    QuillModule.forRoot(), RouterModule, MatProgressBarModule, MatProgressSpinnerModule,
    SocketIoModule.forRoot({ url: environment.host, options: { transports: ['websocket'] } } as SocketIoConfig)
  ],
  providers: [
    HttpService,
    {provide: JWT_OPTIONS, useValue: JWT_OPTIONS},
    JwtHelperService,
    {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true},
    CookieService,
    {provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true},
    UserService,
  ],
  exports: [
    CommonModule, FormsModule,
    ReactiveFormsModule, FormsModule, MatToolbarModule, MatIconModule,
    MatButtonModule, MatInputModule, MatFormFieldModule, MatCardModule,
    MatCheckboxModule, MatSelectModule, MatDialogModule, MatDividerModule,
    MatGridListModule, MatMenuModule, NavbarComponent,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule, MatExpansionModule, MatTabsModule,
    QuillModule, MatProgressBarModule, MatProgressSpinnerModule, LoaderComponent, CardProjectComponent,
    AlertDialogComponent,
    Error404Component,
    LogoutComponent,
    NavbarComponent,
    ConfirmDialogComponent,
    ProfileComponent,
    LoaderComponent, CardCommentComponent,
  ]
})
export class SharedModule { }
