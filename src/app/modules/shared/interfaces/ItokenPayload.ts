export default interface TokenPayload {
  iss: string;
  iat: number;
  exp: number;
  sub: { uuid: string, role: string };
}
