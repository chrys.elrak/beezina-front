import {Component, Inject, OnInit} from '@angular/core';
import {HttpService} from '../../../shared/services/http.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AlertDialogComponent} from '../../../shared/dialogs/alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.scss']
})
export class CreateCommentComponent implements OnInit {
  commentModel = '';
  isLoading = false;

  constructor(
    private httpService: HttpService,
    private dialogRef: MatDialogRef<any>,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {
  }

  ngOnInit(): void {
  }

  sendComment(): void {
    this.isLoading = true;
    const sub = this.httpService
      .patch(`sponsor/project/comment/${this.data.project._id}`, {
        comment: this.commentModel
      })
      .subscribe(res => {
        this.isLoading = false;
        this.dialog.open(AlertDialogComponent, {
          data: {
            message: res.message,
            title: 'Success'
          }
        });
        this.dialogRef.close();
      }, ({error: {message}}) => {
        this.isLoading = false;
        this.dialog.open(AlertDialogComponent, {
          data: {
            message,
            title: 'Comment failed'
          }
        });
      })
      // tslint:disable-next-line:only-arrow-functions
      .add(function(): void {
        sub.unsubscribe();
      });
  }
}
