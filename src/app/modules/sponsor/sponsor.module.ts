import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SponsorRoutingModule} from './sponsor-routing.module';
import {DashboardComponent} from './pages/dashboard/dashboard.component';
import {SharedModule} from '../shared/shared.module';
import { CreateCommentComponent } from './dialogs/create-comment/create-comment.component';


@NgModule({
  declarations: [DashboardComponent, CreateCommentComponent],
  imports: [
    CommonModule,
    SponsorRoutingModule,
    SharedModule
  ]
})
export class SponsorModule {
}
