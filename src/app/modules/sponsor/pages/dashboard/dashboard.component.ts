import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpService} from '../../../shared/services/http.service';
import {MatDialog} from '@angular/material/dialog';
import {CreateCommentComponent} from '../../dialogs/create-comment/create-comment.component';
import {UserService} from '../../../shared/services/user.service';
import {ProjectService} from '../../../shared/services/project.service';
import { Socket } from 'ngx-socket-io';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/modules/auth/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  projects = this.socket.fromEvent<any>('projects');
  // sponsorisedProjects = [];
  user: any;
  isLoading = false;
  subs: Subscription[] = [];

  constructor(
    public httpService: HttpService,
    private dialog: MatDialog,
    private userService: UserService,
    private projectService: ProjectService,
    private authService: AuthService,
    private socket: Socket
  ) {

  }
  ngOnDestroy(): void {
    this.subs.map(sub => sub.unsubscribe());
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.socket.emit('sponsor', this.authService.parseToken().sub.uuid);
    this.subs.push(
      this.projects.subscribe(_ => {
        this.isLoading = false;
      }, err => {
        console.error(err);
        this.isLoading = false;
      })
    );
    this.loadUser();
    // this.loadProject();
  }

  voteProject({project, user}): void {
    this.isLoading = true;
    this.socket.emit('sponsor:vote', {
      project,
      user: this.user
    });
    this.subs.push(
      this.projects.subscribe(_ => {
        this.isLoading = false;
      }, err => {
        console.error(err);
        this.isLoading = false;
      })
    );
    // const sub = this.projectService
    //   .vote(project._id, user ? 'sponsor' : 'public')
    //   .subscribe(res => {
    //     this.loadProject();
    //     this.isLoading = false;
    //   }, error => {
    //     console.log(error);
    //   })
    //   // tslint:disable-next-line:only-arrow-functions
    //   .add(function(): void {
    //     sub.unsubscribe();
    //   });
  }

  supportProject(project: any): void {
    this.isLoading = true;
    this.socket.emit('invest', {
      project,
      user: this.user
    });
    this.subs.push(
      this.projects.subscribe(_ => {
        this.isLoading = false;
      }, err => {
        console.error(err);
        this.isLoading = false;
      })
    );
    // const sub = this.projectService.invest(project._id)
    //   .subscribe(res => {
    //     this.isLoading = false;
    //   }, error => {
    //     this.isLoading = false;
    //   })
    //   // tslint:disable-next-line:only-arrow-functions
    //   .add(function(): void {
    //     sub.unsubscribe();
    //   });
  }

  private loadUser(): void {
    // this.isLoading = true;
    const sub1 = this.userService.getUser()
      .subscribe(user => {
        this.user = user.data;
        // this.isLoading = false;
      }, error => {
        // this.isLoading = false;
      })
      // tslint:disable-next-line:only-arrow-functions
      .add(function(): void {
        sub1.unsubscribe();
      });
  }

  commentProject(project: any): void {
    this.dialog.open(CreateCommentComponent, {
      width: '300px',
      data: {
        project
      }
    });
  }

  private loadProject(): void {
    // this.isLoading = true;
    // const sub = this.projectService.getAll()
    //   .subscribe(res => {
    //     this.projects = res.data;
    //     console.log(this.projects);
    //     this.sponsorisedProjects = this.projects.filter(p => p.investor.find(pp => pp._id === this.user?._id));
    //     this.isLoading = false;
    //   })
    //   // tslint:disable-next-line:only-arrow-functions typedef
    //   .add(function() {
    //     sub.unsubscribe();
    //   });
  }
}
