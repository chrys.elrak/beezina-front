import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../../shared/services/http.service';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {AlertDialogComponent} from '../../../shared/dialogs/alert-dialog/alert-dialog.component';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FormValidation} from '../../../shared/classes/form-validation';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends FormValidation implements OnInit {
  login = new FormControl('',
    [
      Validators.required,
      Validators.minLength(2),
    ]);
  password = new FormControl('',
    [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(100)
    ]);
  hide = true;
  isLoading: any;

  constructor(private httpService: HttpService,
              public dialog: MatDialog,
              private authService: AuthService,
              private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
  }


  loginSubmissionForm(): void {
    if (this.login.invalid || this.password.invalid) {
      return;
    }
    this.isLoading = true;
    const sub = this.httpService.post('/login',
      {login: this.login.value, password: this.password.value})
      .subscribe(res => {
        this.isLoading = false;
        // TODO: redirect to confirm page
        if (res.status === 203) {
          this.dialog.open(AlertDialogComponent, {
            data: {
              message: res.message,
              title: 'Note'
            }
          });
          this.router.navigateByUrl(`/auth/confirm-account?t=${null}&timestamp=${Date.now()}&uuid=${res.data.uuid}`);
          return;
        }
        // TODO: Store token
        this.authService.setToken(res.data);
        this.router.navigateByUrl('/project_owner/dashboard');
        // TODO: Redirect to dashboard
      }, ({error: {message}}) => {
        this.isLoading = false;
        // TODO: Error UI
        this.dialog.open(AlertDialogComponent, {
          data: {
            message,
          }
        });
      })
      // tslint:disable-next-line:only-arrow-functions
      .add(function(): void {
        sub.unsubscribe();
      });
  }


}
