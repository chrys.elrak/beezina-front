import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpService} from '../../../shared/services/http.service';
import {MatDialog} from '@angular/material/dialog';
import {AlertDialogComponent} from '../../../shared/dialogs/alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.scss']
})
export class ConfirmAccountComponent implements OnInit {
  codeEmailValidation: any;
  private token: string;
  private timestamp: string;
  private uuid: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private httpService: HttpService,
    private dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.token = this.route.snapshot.queryParams['t'];
    this.timestamp = this.route.snapshot.queryParams['timestamp'];
    this.uuid = this.route.snapshot.queryParams['uuid'];
    //TODO: Check server
  }

  sendEmailVerification() {
    const sub = this.httpService.post(`/register/confirmation-code/${this.uuid}`,
      {code: this.codeEmailValidation})
      .subscribe(({message}) => {
        const sub1 = this.dialog.open(AlertDialogComponent, {
          data: {
            message,
            title: 'Success'
          }
        }).afterOpened()
          .subscribe(() => {
            this.codeEmailValidation = '';
            return this.router.navigateByUrl('/auth/login');
          })
          .add(function() {
            sub1.unsubscribe();
          });
      }, ({error: {message}}) => {
        const sub1 = this.dialog.open(AlertDialogComponent, {
          data: {
            message
          }
        }).afterOpened()
          .subscribe(() => {
            this.codeEmailValidation = '';
          })
          .add(function() {
            sub1.unsubscribe();
          });
      })
      .add(function() {
        sub.unsubscribe();
      });
  }
}
