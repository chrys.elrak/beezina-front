import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../../shared/services/http.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {AlertDialogComponent} from '../../../shared/dialogs/alert-dialog/alert-dialog.component';
import {ActivatedRoute, Router} from '@angular/router';
import {FormValidation} from '../../../shared/classes/form-validation';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends FormValidation implements OnInit {
  registerForm: FormGroup;
  username: string;
  hide = true;
  roles = [{
    viewValue: 'Sponsor',
    value: 'sponsor',
    hint: 'blabla'
  }, {
    viewValue: 'Project owner',
    value: 'project_owner',
    hint: 'xoxo'
  }];
  hintAccount: any;
  genders = [{
    viewValue: 'Male',
    value: 'male'
  }, {
    viewValue: 'Female',
    value: 'female'
  }];
  errorMessage: any;
  confirmPassword: FormControl;
  isLoading: any;

  constructor(private httpService: HttpService,
              private fb: FormBuilder,
              private dialog: MatDialog,
              private router: Router,
              private activatedRoute: ActivatedRoute
  ) {
    super();
    this.registerForm = this.fb.group({
      username: ['', [
        Validators.required,
        Validators.minLength(2),
        Validators.pattern('[a-zA-Z0-9_-]*')
      ]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      phoneNumber: [''],
      email: ['', [Validators.required, Validators.email]],
      role: [this.activatedRoute.snapshot.queryParams.role || this.roles[0].value, Validators.required],
      gender: [this.genders[0].value, Validators.required],
    },);

    this.confirmPassword = new FormControl('', [
      (control) => {
        if (this.registerForm.get('password').value !== control.value) {
          return {'mismatch': true};
        }
        return null;
      }
    ]);
  }

  ngOnInit(): void {
  }

  registerSubmissionForm() {
    const body = {
      password: this.registerForm.get('password').value,
      username: this.registerForm.get('username').value,
      email: this.registerForm.get('email').value,
      phone: this.registerForm.get('phoneNumber').value,
      gender: this.registerForm.get('gender').value,
      role: this.registerForm.get('role').value,
    };
    if (body.phone === '') {
      delete body.phone;
    }
    this.isLoading = true;
    const sub = this.httpService.post('/register', body)
      .subscribe(res => {
        // TODO: Response
        this.isLoading = false;
        this.dialog.open(AlertDialogComponent, {
          data: {message: res.message, title: 'Success'}
        });
        return this.router.navigateByUrl(
          `/auth/confirm-account?t=${res.tokenPayload || ''}&timestamp=${Date.now()}&uuid=${res.data.user_id || ''}`
        );
      }, ({error: {message}}) => {
        this.isLoading = false;
        this.dialog.open(AlertDialogComponent, {
          data: {message}
        });
      })
      .add(function() {
        sub.unsubscribe();
      });

  }
}
