import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(public authService: AuthService, public router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    if (!this.authService.isAuthenticated()) {
      this.authService.logout();
      this.router.navigateByUrl('/auth/login');
      return false;
    }

    const expectedRole = route.data.role;
    if (!expectedRole) {
      return true;
    }

    const tokenPayload = this.authService.parseToken();

    if (tokenPayload.sub.role !== expectedRole) {
      this.router.navigateByUrl(`/${tokenPayload.sub.role}`);
      return false;
    }
    return true;
  }

}
