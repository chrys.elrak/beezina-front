import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {CookieService} from 'ngx-cookie-service';
import {StorageService} from '../../shared/services/storage.service';
import {HttpService} from '../../shared/services/http.service';
import {Token} from '../../shared/models/token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private storageKey = 'bz-token';
  private renewalInProgress = false;

  constructor(
    private jwtService: JwtHelperService,
    private cookieService: CookieService,
    private storage: StorageService,
    private httpService: HttpService
  ) {
  }

  token(): Token {
    return this.storage.getObject(this.storageKey) as Token;
  }

  getStorageKey(): string {
    return this.storageKey;
  }

  parseToken(): any {
    const token = this.token();
    if (!token) {
      return false;
    }
    try {
      return this.jwtService.decodeToken(token.token);
    } catch (err) {
      this.logout();
    }
  }

  setToken(token): void {
    if (typeof token === 'string') {
      token = {token};
    }
    this.storage.setObject(this.storageKey, token);
  }

  isAuthenticated(): boolean {
    const token = this.token();
    if (!token) {
      return false;
    }
    return !this.jwtService.isTokenExpired(token?.token);
  }


  logout(): void {
    // TODO clear all storage
    this.storage.setObject(this.storageKey, null);
    this.cookieService.deleteAll();
  }

  checkRenewal(): any {
    if (this.renewalInProgress === true) {
      return false;
    }
    // Check if renewal is needed
    const deltaExp = this.getDeltaExp();
    if (deltaExp > 0 && deltaExp < (30 * 60)) {
      this.renewalInProgress = true;
      this.httpService.get(`/renew`).subscribe(token => {
        this.storage.setObject(this.storageKey, token);
      }).add(() => {
        this.renewalInProgress = false;
      });
    }
  }

  getDeltaExp(): number {
    return this.parseToken().exp - Math.round((new Date()).getTime() / 1000);
  }

  getRole(): string {
    return this.jwtService.decodeToken(this.token().token).sub.role;
  }
}
