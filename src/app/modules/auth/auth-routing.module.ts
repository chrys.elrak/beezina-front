import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegisterComponent} from './pages/register/register.component';
import {LoginComponent} from './pages/login/login.component';
import {ConfirmAccountComponent} from './pages/confirm-account/confirm-account.component';

const routes: Routes = [
  {
    path: 'register', component: RegisterComponent, pathMatch: 'full',
  },
  {
    path: 'login', component: LoginComponent, pathMatch: 'full',
  },
  {
    path: 'confirm-account', component: ConfirmAccountComponent, pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
