import {NgModule} from '@angular/core';
import {RegisterComponent} from './pages/register/register.component';
import {SharedModule} from '../shared/shared.module';
import {AuthRoutingModule} from './auth-routing.module';
import {LoginComponent} from './pages/login/login.component';
import {ConfirmAccountComponent} from './pages/confirm-account/confirm-account.component';


@NgModule({
  declarations: [RegisterComponent, LoginComponent, ConfirmAccountComponent],
  providers: [],
  imports: [
    SharedModule,
    AuthRoutingModule
  ]
})
export class AuthModule {
}
