import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../auth/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  projectOwner: any;
  getStarted: string;
  connected: boolean;

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    this.getStarted = this.authService.isAuthenticated() ? 'Dashboard' : 'Get started';
    this.connected = this.authService.isAuthenticated();
  }

}
