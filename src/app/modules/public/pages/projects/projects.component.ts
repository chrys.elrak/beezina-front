import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../../../shared/services/project.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  projects = [];
  isLoading = false;

  constructor(
    private projectService: ProjectService
  ) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    const sub = this.projectService
      .getWithoutDetail()
      .subscribe(res => {
        this.projects = res.data;
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
      })
      // tslint:disable-next-line:only-arrow-functions
      .add(function(): void {
        sub.unsubscribe();
      });
  }

}
