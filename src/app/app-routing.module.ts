import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './modules/auth/services/auth.guard';
import {AnonymousGuard} from './modules/auth/services/anonymous.guard';
import {Error404Component} from './modules/shared/pages/error404/error404.component';
import {LogoutComponent} from './modules/shared/components/logout/logout.component';
import {HomeComponent} from './modules/public/pages/home/home.component';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
    canActivate: [AnonymousGuard]
  },
  {
    path: 'sponsor',
    loadChildren: () => import('./modules/sponsor/sponsor.module').then(m => m.SponsorModule),
    canActivate: [AuthGuard],
    data: {
      role: 'sponsor'
    }
  },
  {
    path: 'project_owner',
    loadChildren: () => import('./modules/project-owner/project-owner.module').then(m => m.ProjectOwnerModule),
    canActivate: [AuthGuard],
    data: {
      role: 'project_owner'
    }
  },
  {
    path: 'logout',
    pathMatch: 'full',
    component: LogoutComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'projects',
    pathMatch: 'full',
    loadChildren: () => import('./modules/public/public.module').then(m => m.PublicModule)
  },
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    component: Error404Component
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
